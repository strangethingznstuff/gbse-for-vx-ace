#==============================================================================
#TOP OF ALL SCRIPTS EVEN ENGINE SCRIPTS!!!!!!
#
#  GBSE Exporter - Good Bye Script Editor Exporter (By Soulite)
#  
#  Feel free to use this for commercial and non commercial projects
#  Code is offered under the MIT License - https://opensource.org/license/mit
#
#==============================================================================

#INSTRUCTIONS
#1 - Add a folder "Scripts" (CASE SENSETIVE) to your base project folder
#2 - put GBSE Exporter at top of project
#3 - run your game and let it export the scripts
#4 - put # at the begining of the exported file from this script (comment out)
#5 - put GBSE at top of project
#6 - enjoy scripting in any editor you please!



ENGINE_SEPARATOR = "▼ Materials"

#$RGSS_SCRIPTS[i][0] -> script ID? (seemingly random 8 digit number, same on restart)
#$RGSS_SCRIPTS[i][1] -> script NAME
#$RGSS_SCRIPTS[i][1] -> script METADATA?
#$RGSS_SCRIPTS[i][3] -> script CODE (works being changed regardless of the rest)


#Engine scripts will be packed as 10000,10010, etc.
#Material scripts will be packed as 20000,20010, etc.
#this way if old script orders need to be changed, some space is in-between

#find engine separator
engine_separator_index = 0
for i in 0 ... $RGSS_SCRIPTS.length
  if $RGSS_SCRIPTS[i][1] == ENGINE_SEPARATOR
    engine_separator_index = i
    break
  end
end

script_index = 10000

for i in 0 ... $RGSS_SCRIPTS.length
  if i == engine_separator_index
    script_index = 20000
  end
  if $RGSS_SCRIPTS[i][3] != "" && $RGSS_SCRIPTS[i][3] != nil
    filepath = "Scripts/"+script_index.to_s+" - "+$RGSS_SCRIPTS[i][1]+".rb"
    filepath = filepath.gsub(/[\u0080-\uffff]/, "")
    code = $RGSS_SCRIPTS[i][3]
    code.gsub!("\r\n", "\n")
    File.open(filepath, 'w') { |file| file.write(code) }
    script_index += 10
  end
end