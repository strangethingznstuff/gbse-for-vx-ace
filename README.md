# GBSE for VX Ace

Good Bye Script Editor removes the need for RPG Maker VX Ace's built in script editor, instead moving scripts to a folder where they can be edited with the IDE of your choice!

## Installation

1 - Add a folder "Scripts" (CASE SENSETIVE) to your base project folder

2 - put GBSE Exporter at top of project

3 - run your game and let it export the scripts

4 - put # at the begining of the exported file from this script (comment out)

5 - put GBSE at top of project

6 - enjoy scripting in any editor you please!

## Usage

Use "#" to comment out scripts

Scripts are loaded in order of line number (the biggest number code will overwrite small number code).

## Authors and acknowledgment

Created by Soulite/Hopperisabunny

Script disabling with # inspired by Rycochet's Disable Scripts v1.0 plugin

## License

tl;dr Feel free to use this for commercial and non commercial projects

Code is offered under the MIT License - https://opensource.org/license/mit