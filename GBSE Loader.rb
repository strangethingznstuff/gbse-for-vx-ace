#==============================================================================
#TOP OF ALL SCRIPTS EVEN ENGINE SCRIPTS!!!!!!
#
#  GBSE - Good Bye Script Editor (By Soulite)
#  
#  Feel free to use this for commercial and non commercial projects
#  Code is offered under the MIT License - https://opensource.org/license/mit
#
#==============================================================================

#INSTRUCTIONS
#1 - Add a folder "Scripts" (CASE SENSETIVE) to your base project folder
#2 - put GBSE Exporter at top of project
#3 - run your game and let it export the scripts
#4 - put # at the begining of the exported file from this script (comment out)
#5 - put GBSE at top of project
#6 - enjoy scripting in any editor you please!

script_file_list = Dir.entries("./Scripts/")
script_file_list = script_file_list.sort
script_file_list = script_file_list[2,script_file_list.length]
script_file_list.delete_if {|name| name.index("#") != nil}

#sort files based on ID
CHECKLOAD = false

loaded_script_id = 1

for i in 0 ... script_file_list.length
  code = ""
  File.open("Scripts/"+script_file_list[i], 'r') { |file| code = file.read() }
  if CHECKLOAD
    if code != ""
      p "loaded script "+script_file_list[i]+" successfully!"
    else
      p "WARNING: script "+script_file_list[i]+" was empty or failed to load."
    end
  end
  $RGSS_SCRIPTS[loaded_script_id][3] = code
  $RGSS_SCRIPTS[loaded_script_id][1] = script_file_list[i]
  loaded_script_id += 1
end
for i in loaded_script_id ... $RGSS_SCRIPTS.length
  $RGSS_SCRIPTS[loaded_script_id][3] = ""
  $RGSS_SCRIPTS[loaded_script_id][1] = ""
end